/* SPDX-License-Identifier: LGPL-2.1-or-later */
#include <libresp.h>
#include <libresp-private.h>

#include <errno.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

struct posix_io {
  struct resp_io base;
  int r, w;
};

static ssize_t posix_read(struct resp_io* r, char* buf, size_t len) {
  struct posix_io* io = (struct posix_io*)r;
  return read(io->r, buf, len);
}

static ssize_t posix_write(struct resp_io* r, const void* buf, size_t len) {
  struct posix_io* io = (struct posix_io*)r;
  return write(io->w, buf, len);
}

static void posix_close(struct resp_io* r) {
  struct posix_io* io = (struct posix_io*)r;

  close(io->r);

  if(io->w != io->r)
    close(io->w);
}

static const struct resp_io_actions posix_actions = {
  .read = posix_read,
  .write = posix_write,
  .close = posix_close,
};

static inline int tcp_socktype(int type) {
  return type
#if defined(SOCK_CLOEXEC)
    | SOCK_CLOEXEC
#endif
    ;
}

static inline int tcp_set_cloexec(int fd) {
  (void)fd;

#if !defined(SOCK_CLOEXEC)
  int flags = fcntl(fd, F_GETFD);

  if(flags == -1)
    return 1;

  flags |= FD_CLOEXEC;

  if(fcntl(fd, F_SETFD, flags) == -1)
    return 1;
#endif

  return 0;
}

static int tcp_create_socket(const char* addr, const char* port) {
  int err, fd = -1;
  struct addrinfo hints, *result, *iter;

  memset(&hints, 0, sizeof(struct addrinfo));
  hints.ai_family = AF_UNSPEC;
  hints.ai_socktype = SOCK_STREAM;

  if((err = getaddrinfo(addr, port, &hints, &result))) {
    fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(err));
    return -1;
  }

  for(iter = result; iter != NULL; iter = iter->ai_next) {
    fd = socket(iter->ai_family,
                tcp_socktype(iter->ai_socktype),
                iter->ai_protocol);

    if(fd == -1)
      continue;

    if(connect(fd, iter->ai_addr, iter->ai_addrlen) != -1)
      break;

    close(fd);
    fd = -1;
  }

  if(result)
    freeaddrinfo(result);

  if(fd == -1)
    return -1;

  if(tcp_set_cloexec(fd)) {
    err = errno;
    close(fd);
    errno = err;
    return -1;
  }

  return fd;
}

struct resp_io* resp_io_tcp_init(const char* addr, const char* port) {
  int fd = tcp_create_socket(addr, port);

  if(fd == -1)
    return NULL;

  struct resp_io* io = resp_io_fd_init(fd, fd);

  if(io == NULL) {
    int err = errno;
    close(fd);
    errno = err;
    return NULL;
  }

  return io;
}

struct resp_io* resp_io_socket_init(int r, int w) {
  return resp_io_fd_init(r, w);
}

struct resp_io* resp_io_fd_init(int r, int w) {
  struct posix_io* io = calloc(1, sizeof(struct posix_io));

  if(io == NULL)
    return NULL;

  io->base.actions = &posix_actions;
  io->r = r;
  io->w = w;

  return (struct resp_io*)io;
}

void resp_io_get_socket(struct resp_io* r, int* fd) {
  resp_io_get_fd(r, fd);
}

void resp_io_get_fd(struct resp_io* r, int* fd) {
  struct posix_io* io = (struct posix_io*)r;

  fd[0] = io->r;
  fd[1] = io->w;
}
