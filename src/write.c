/* SPDX-License-Identifier: LGPL-2.1-or-later */
#include <libresp.h>
#include <libresp-private.h>

#include <assert.h>
#include <inttypes.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define PRI_RESPINT PRId64

static int encode(struct resp_io*, struct resp_window*, const struct resp_msg*);

static int flush_buffer(struct resp_io* io, struct resp_window* w) {
  while(w->begin < w->end) {
    ssize_t nr = resp_io_write(io, w->begin, w->end - w->begin);

    if(nr == -1)
      return -1;

    if(nr == 0) {
      errno = 0;
      return -1;
    }

    w->begin += nr;
  }

  assert(w->begin == w->end);

  w->begin = w->end = w->buf;

  return 0;
}

static int write_buffer(struct resp_io* io, struct resp_window* w, const char* buf, size_t len) {
  const char* end = buf + len;

  while(buf < end) {
    size_t rem = min(end - buf, w->buf + RESP_WINDOW_SIZE - w->end);

    memcpy(w->end, buf, rem);

    buf += rem;
    w->end += rem;

    if(w->end == w->buf + RESP_WINDOW_SIZE) {
      if(flush_buffer(io, w) == -1)
        return -1;

      /* TODO: This is a good spot to shortcut directly to resp_io_write if
         the remaining data is large, to avoid needlessly copying to the
         window. */
    }
  }

  assert(buf == end);

  return 0;
}

static int mk_null(struct resp_io* io, struct resp_window* w) {
  static const char buf[] = "$-1\r\n";
  return write_buffer(io, w, buf, sizeof(buf) - 1);
}

static int mk_integer(struct resp_io* io, struct resp_window* w, char prefix, resp_int v) {
  enum { max_len = 24 }; /* Longest required is for LLONG_MIN, at 24 chars */
  char buf[24];
  int nr = snprintf(buf, 24, "%c%"PRI_RESPINT"\r\n", prefix, v);
  assert(nr < 24);
  return write_buffer(io, w, buf, nr);
}

static int mk_string(struct resp_io* io, struct resp_window* w, char prefix, size_t len, const char* buf) {
  int r;

  if(prefix != '\0' && ((r = write_buffer(io, w, &prefix, 1)) != 0))
    return r;

  if((r = write_buffer(io, w, buf, len)) != 0)
    return r;

  return write_buffer(io, w, "\r\n", 2);
}

static int mk_bulk(struct resp_io* io, struct resp_window* w, size_t len, const char* buf) {
  int r;

  if((r = mk_integer(io, w, '$', len)) != 0)
    return r;

  if((r = write_buffer(io, w, buf, len)) != 0)
    return r;

  return write_buffer(io, w, "\r\n", 2);
}

static int mk_array(struct resp_io* io, struct resp_window* w, size_t num, struct resp_msg* const * array) {
  int r;

  if((r = mk_integer(io, w, '*', num)) != 0)
    return r;

  for(size_t i = 0; i < num; i++)
    if((r = encode(io, w, array[i])) != 0)
      return r;

  return 0;
}

static int encode(struct resp_io* io, struct resp_window* w, const struct resp_msg* msg) {
  switch(msg->type) {
    case resp_type_null:
      return mk_null(io, w);
    case resp_type_integer:
      return mk_integer(io, w, ':', msg->integer);
    case resp_type_simple_string:
      return mk_string(io, w, '+', msg->length, msg->string);
    case resp_type_error:
      return mk_string(io, w, '-', msg->length, msg->string);
    case resp_type_inline_command:
      return mk_string(io, w, '\0', msg->length, msg->string);
    case resp_type_bulk_string:
      return mk_bulk(io, w, msg->length, msg->string);
    case resp_type_array:
      return mk_array(io, w, msg->num, msg->array);
  }

  errno = EINVAL;
  return -1;
}

int resp_write(struct resp_ctx* ctx, const struct resp_msg* msg) {
  struct resp_window w;
  w.begin = w.end = w.buf;

  int r = encode(ctx->io, &w, msg);

  if(r == -1)
    return r;

  return flush_buffer(ctx->io, &w);
}

/** Writes a message using an encoding format string
 *
 * fmt is a character sequence representing an array of types. The resulting
 * message will be encoded in RESP as an array of bulk strings, then written to
 * the stream.
 *
 * Format:
 *  b : (size_t, const void*) bulk string. Arguments are the length of the data
 *    and a pointer to the data
 *
 *  s : (const char*) simple string. Argument is a pointer to a null-terminated
 *    string. Strings will not be checked for invalid characters.
 *
 *  d : (resp_int) integer. Read as a full 64-bit argument
 *
 *  i : (int) integer. Read as the platform int type
 *
 * Arrays, errors, and inline commands are not supported. For complex messages,
 * resp_write may be used.
 *
 * Example:
 *
 *   char b[] = {0,32,64,127};
 *   resp_int d = 19;
 *   int i = 42;
 *   resp_writef(ctx, "sdbi", "PING", d, sizeof(b), b, i);
 *
 * The above code will be encoded as:
 *
 *   *4\r\n$4\r\nPING\r\n$2\r\n19\r\n$4\r\n\x00\x20\x40\x7F\r\n$2\r\n42\r\n
 */
int resp_writef(struct resp_ctx* ctx, const char* fmt, ...) {
  struct resp_window w;
  w.begin = w.end = w.buf;

  int r;
  size_t len = strlen(fmt);

  if((r = mk_integer(ctx->io, &w, '*', len)))
    return r;

  va_list args;
  va_start(args, fmt);

  for(; *fmt != '\0'; fmt++) {
    char int_data[24];
    const char* data;
    size_t size;
    switch(*fmt) {
      case 'b':
        size = va_arg(args, size_t);
        data = va_arg(args, char*);
        break;
      case 's':
        data = va_arg(args, char*);
        size = strlen(data);
        break;
      case 'd':
        size = snprintf(int_data, 24, "%"PRI_RESPINT, va_arg(args, resp_int));
        data = int_data;
        break;
      case 'i':
        size = snprintf(int_data, 24, "%"PRI_RESPINT, (resp_int)va_arg(args, int));
        data = int_data;
        break;
      default:
        errno = EINVAL;
        return 1;
    }

    if((r = mk_bulk(ctx->io, &w, size, data)))
      return r;
  }

  va_end(args);

  return flush_buffer(ctx->io, &w);
}
