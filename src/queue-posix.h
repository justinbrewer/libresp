/* SPDX-License-Identifier: LGPL-2.1-or-later */
#ifndef __QUEUE_POSIX_H__
#define __QUEUE_POSIX_H__

#ifndef __QUEUE_H__
#error "Do not include queue-*.h directly. Use queue.h"
#endif

#include <errno.h>
#include <pthread.h>
#include <time.h>

/* pthreads-w32 does not support CLOCK_MONOTONIC for sleeping */
#if defined(RESP_HOST_WIN32)
#define QUEUE_CLOCK CLOCK_REALTIME
#else
#define QUEUE_CLOCK CLOCK_MONOTONIC
#endif

static inline int timespec_cmp(const struct timespec* l, const struct timespec* r) {
  return l->tv_sec < r->tv_sec || (l->tv_sec == r->tv_sec && l->tv_nsec < r->tv_nsec);
}

/* pthread_mutexattr_setclock does not yet exist */
static int pthread_mutex_timedlock_np(pthread_mutex_t* mtx, const struct timespec* abs_timeout) {
#if QUEUE_CLOCK != CLOCK_REALTIME
  int r;
  struct timespec now;

  do {
    r = pthread_mutex_trylock(mtx);

    switch(r) {
      case 0:
        return 0;
      case EBUSY:
        break;
      default:
        return r;
    }

    sched_yield();

    clock_gettime(QUEUE_CLOCK, &now);
  } while(timespec_cmp(&now, abs_timeout));

  return ETIMEDOUT;
#else
  return pthread_mutex_timedlock(mtx, abs_timeout);
#endif
}

struct queue {
  pthread_cond_t rcv, wcv;
  pthread_mutex_t mtx;
  void* v;
};

static int queue_init(struct queue* q) {
  int err;
  pthread_condattr_t attr;

  q->v = NULL;

  if((err = pthread_condattr_init(&attr)))
    goto error_cond;

  if((err = pthread_condattr_setclock(&attr, QUEUE_CLOCK)))
    goto error_cond;

  if((err = pthread_cond_init(&q->rcv, &attr)))
    goto error_cond;

  if((err = pthread_cond_init(&q->wcv, &attr)))
    goto error_wcv;

  if((err = pthread_mutex_init(&q->mtx, NULL)))
    goto error_mtx;

  pthread_condattr_destroy(&attr);
  return 0;

error_mtx:
  pthread_cond_destroy(&q->wcv);

error_wcv:
  pthread_cond_destroy(&q->rcv);

error_cond:
  pthread_condattr_destroy(&attr);

  errno = err;
  return -1;
}

static void queue_destroy(struct queue* q) {
  pthread_mutex_destroy(&q->mtx);
  pthread_cond_destroy(&q->wcv);
  pthread_cond_destroy(&q->rcv);
}

static void* queue_pop(struct queue* q) {
  int err;
  void* r = NULL;

  if((err = pthread_mutex_lock(&q->mtx)))
    goto error;

  while(q->v == NULL)
    if((err = pthread_cond_wait(&q->rcv, &q->mtx)))
      goto error_locked;

  r = q->v;
  q->v = NULL;

  pthread_cond_signal(&q->wcv);

  pthread_mutex_unlock(&q->mtx);
  return r;

error_locked:
  pthread_mutex_unlock(&q->mtx);

error:
  errno = err;
  return NULL;
}

static void* queue_trypop(struct queue* q) {
  int err;
  void* r = NULL;

  if((err = pthread_mutex_trylock(&q->mtx)))
    goto error;

  if(q->v == NULL) {
    err = EBUSY;
    goto error_locked;
  }

  r = q->v;
  q->v = NULL;

  pthread_cond_signal(&q->wcv);

  pthread_mutex_unlock(&q->mtx);
  return r;

error_locked:
  pthread_mutex_unlock(&q->mtx);

error:
  errno = err;
  return NULL;
}

static void* queue_timedpop(struct queue* q, const struct timespec* abs_timeout) {
  int err;
  void* r;

  if((err = pthread_mutex_timedlock_np(&q->mtx, abs_timeout)))
    goto error;

  while(q->v == NULL)
    if((err = pthread_cond_timedwait(&q->rcv, &q->mtx, abs_timeout)))
      goto error_locked;

  r = q->v;
  q->v = NULL;

  pthread_cond_signal(&q->wcv);

  pthread_mutex_unlock(&q->mtx);
  return r;

error_locked:
  pthread_mutex_unlock(&q->mtx);

error:
  errno = err;
  return NULL;
}

static int queue_push(struct queue* q, void* v) {
  int err;

  if((err = pthread_mutex_lock(&q->mtx)))
    goto error;

  while(q->v != NULL)
    if((err = pthread_cond_wait(&q->wcv, &q->mtx)))
      goto error_locked;

  q->v = v;

  pthread_cond_signal(&q->rcv);

  pthread_mutex_unlock(&q->mtx);
  return 0;

error_locked:
  pthread_mutex_unlock(&q->mtx);

error:
  return err;
}

static int queue_trypush(struct queue* q, void* v) {
  int err;

  if((err = pthread_mutex_trylock(&q->mtx)))
    goto error;

  if(q->v != NULL) {
    err = EBUSY;
    goto error_locked;
  }

  q->v = v;

  pthread_cond_signal(&q->rcv);

  pthread_mutex_unlock(&q->mtx);
  return 0;

error_locked:
  pthread_mutex_unlock(&q->mtx);

error:
  return err;
}

static int queue_timedpush(struct queue* q, void* v, const struct timespec* abs_timeout) {
  int err;

  if((err = pthread_mutex_timedlock_np(&q->mtx, abs_timeout)))
    goto error;

  while(q->v != NULL)
    if((err = pthread_cond_timedwait(&q->wcv, &q->mtx, abs_timeout)))
      goto error_locked;

  q->v = v;

  pthread_cond_signal(&q->rcv);

  pthread_mutex_unlock(&q->mtx);
  return 0;

error_locked:
  pthread_mutex_unlock(&q->mtx);

error:
  return err;
}

#endif
