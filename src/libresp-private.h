/* SPDX-License-Identifier: LGPL-2.1-or-later */
#ifndef __LIBRESP_PRIVATE_H__
#define __LIBRESP_PRIVATE_H__

#include <libresp.h>
#include <queue.h>

#include <pthread.h>
#include <stdatomic.h>
#include <stdint.h>

enum resp_attr_flags {
  resp_attr_blocking = 1 << 0,
  resp_attr_timeout  = 1 << 1,
  resp_attr_readthrd = 1 << 2,
};

struct resp_attr {
  uint32_t flags;
  struct timespec timeout;
};

extern const struct resp_attr resp_default_attr;

#define RESP_WINDOW_SIZE 4096

struct resp_window {
  char *begin, *end;
  char buf[RESP_WINDOW_SIZE];
};

struct resp_ctx {
  struct resp_attr attr;
  struct resp_io* io;

  union {
    struct {
      pthread_t thread;
      struct queue queue;
    };

    struct {
      pthread_mutex_t read_mutex;
      struct resp_window* read_window;
    };
  };

  atomic_int error;
};

static inline
ssize_t resp_io_read(struct resp_io* r, char* buf, size_t len) {
  return r->actions->read(r, buf, len);
}

static inline
ssize_t resp_io_write(struct resp_io* r, const void* buf, size_t len) {
  return r->actions->write(r, buf, len);
}

static inline
void resp_io_close(struct resp_io* r) {
  r->actions->close(r);
}

void resp_io_get_socket(struct resp_io* r, resp_socket* fd);
void resp_io_get_fd(struct resp_io* r, resp_fd* fd);

const char* resp_token_read(struct resp_window* w, struct resp_io* r, size_t* len, int* is_huge);
int resp_token_read_block(struct resp_window* w, struct resp_io* r, char* buf, size_t len);
char* resp_token_huge(struct resp_window* w, struct resp_io* r,
    char* buf, char* begin, size_t size, size_t skip, size_t* len);

struct resp_msg* resp_msg_read(struct resp_window* w, struct resp_io* r);

#endif
