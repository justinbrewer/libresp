/* SPDX-License-Identifier: LGPL-2.1-or-later */
#include <libresp.h>
#include <libresp-private.h>

#include <stdlib.h>

void resp_io_free(struct resp_io* r) {
  if(r == NULL)
    return;

  resp_io_close(r);

  free(r);
}
