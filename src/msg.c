/* SPDX-License-Identifier: LGPL-2.1-or-later */
#include <libresp.h>
#include <libresp-private.h>

#include <ctype.h>
#include <errno.h>
#include <limits.h>
#include <stdlib.h>
#include <string.h>

#define RESP_BULK_STRING_MAX (1 << 29)

static struct resp_msg* mk_simple_string(enum resp_type type, const char* token, size_t len) {
  struct resp_msg* msg = malloc(sizeof(struct resp_msg) + len + 1);

  if(msg == NULL)
    return NULL;

  msg->type = type;
  msg->length = len;
  memcpy(msg->string, token, len);
  msg->string[len] = '\0';

  return msg;
}

static int parse_int(const char* token, resp_int* v) {
  resp_int n = strtoll(token, NULL, 10);

  if(errno == ERANGE && (n == LLONG_MIN || n == LLONG_MAX)) {
    errno = EPROTO;
    return 1;
  }

  *v = n;
  return 0;
}

static struct resp_msg* mk_integer(const char* token) {
  struct resp_msg* msg = malloc(sizeof(struct resp_msg));

  msg->type = resp_type_integer;

  if(parse_int(token, &msg->integer) != 0) {
    int err = errno;
    free(msg);
    errno = err;
    return NULL;
  }

  return msg;
}

static struct resp_msg* mk_null() {
  struct resp_msg* msg = malloc(sizeof(struct resp_msg));

  if(msg == NULL)
    return NULL;

  msg->type = resp_type_null;

  /* TODO: Set this to indicate array or bulk null? Spec suggests that it is
     not important, so for now, it will be zero. */
  msg->null_type = 0;

  return msg;
}

static struct resp_msg* mk_bulk_string(struct resp_window* w, struct resp_io* r, const char* token) {
  resp_int n;

  if(parse_int(token, &n) != 0)
    return NULL;

  if(n == -1)
    return mk_null();

  if(n > RESP_BULK_STRING_MAX || n < -1) {
    errno = EPROTO;
    return NULL;
  }

  /* Add two bytes to leave room for the tokenizer to check the delimiter */
  struct resp_msg* msg = malloc(sizeof(struct resp_msg) + n + 2);

  if(msg == NULL)
    return NULL;

  msg->type = resp_type_bulk_string;
  msg->length = n;

  if(resp_token_read_block(w, r, msg->string, n + 2) != 0) {
    int err = errno;
    free(msg);
    errno = err;
    return NULL;
  }

  return msg;
}

static struct resp_msg* mk_array(struct resp_window* w, struct resp_io* r, const char* token) {
  resp_int n;

  if(parse_int(token, &n) != 0)
    return NULL;

  if(n == -1)
    return mk_null();

  if(n < -1) {
    errno = EPROTO;
    return NULL;
  }

  struct resp_msg* msg = malloc(sizeof(struct resp_msg) + n * sizeof(struct resp_msg*));

  if(msg == NULL)
    return NULL;

  msg->type = resp_type_array;
  msg->num = n;

  size_t i;
  for(i = 0; i < msg->num; i++)
    if((msg->array[i] = resp_msg_read(w, r)) == NULL)
      goto error;

  return msg;

  int err;
error:
  err = errno;

  for(size_t j = 0; j < i; j++)
    free(msg->array[j]);

  free(msg);

  errno = err;
  return NULL;
}

static struct resp_msg* mk_huge(struct resp_window* w, struct resp_io* r, enum resp_type type, size_t skip) {
  size_t len;
  struct resp_msg* msg = malloc(RESP_WINDOW_SIZE * 2);

  if(msg == NULL)
    return NULL;

  msg = (struct resp_msg*)resp_token_huge(w, r, (char*)msg, msg->string, RESP_WINDOW_SIZE * 2, skip, &len);

  if(msg == NULL)
    return NULL;

  msg->type = type;
  msg->length = len;

  return msg;
}

struct resp_msg* resp_msg_read(struct resp_window* w, struct resp_io* r) {
  int is_huge;
  size_t len;
  const char* token = resp_token_read(w, r, &len, &is_huge);

  if(token == NULL) {
    if(is_huge) {
      switch(w->buf[0]) {
        case '+':
          return mk_huge(w, r, resp_type_simple_string, 1);
        case '-':
          return mk_huge(w, r, resp_type_error, 1);
        case ':':
        case '$':
        case '*':
          errno = EPROTO;
          return NULL;
      }

      if(isalpha(w->buf[0]))
        return mk_huge(w, r, resp_type_inline_command, 0);

      errno = EPROTO;
    }

    return NULL;
  }

  if(len == 0) {
    errno = EPROTO;
    return NULL;
  }

  switch(token[0]) {
    case '+':
      return mk_simple_string(resp_type_simple_string, token + 1, len - 1);
    case '-':
      return mk_simple_string(resp_type_error, token + 1, len - 1);
    case ':':
      return mk_integer(token + 1);
    case '$':
      return mk_bulk_string(w, r, token + 1);
    case '*':
      return mk_array(w, r, token + 1);
    default:
      break;
  }

  if(isalpha(token[0]))
    return mk_simple_string(resp_type_inline_command, token, len);

  errno = EPROTO;
  return NULL;
}

/** Free a resp_msg object.
 * 
 * NULL is a valid noop.
 */
void resp_msg_free(struct resp_msg* msg) {
  if(!msg) {
    return;
  }

  switch(msg->type) {
    case resp_type_null:
    case resp_type_simple_string:
    case resp_type_error:
    case resp_type_integer:
    case resp_type_bulk_string:
    case resp_type_inline_command:
      break;

    case resp_type_array:
      for(size_t i = 0; i < msg->length; i++) {
        resp_msg_free(msg->array[i]);
      }
      break;
  }

  free(msg);
}
