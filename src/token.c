/* SPDX-License-Identifier: LGPL-2.1-or-later */
#include <libresp.h>
#include <libresp-private.h>

#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>

static inline void sanity_check(struct resp_window* w) {
  (void)w;

  assert(w->buf <= w->begin);
  assert(w->begin <= w->buf + RESP_WINDOW_SIZE);
  assert(w->buf <= w->end);
  assert(w->end <= w->buf + RESP_WINDOW_SIZE);
  assert(w->begin <= w->end);
}

static void make_room(struct resp_window* w) {
  size_t n = w->end - w->begin;
  memmove(w->buf, w->begin, n);
  w->begin = w->buf;
  w->end = w->buf + n;
}

static char* find_delim(char* begin, char* end) {
  for(; begin != end; begin++) {
    if(*begin == '\r') {
      if(begin + 1 == end)
        break;

      if(*(begin + 1) == '\n')
        return begin;

      /* Found a lone \r in a binary-unsafe context */
      errno = EPROTO;
      return NULL;
    }

    if(*begin == '\n') {
      /* Found a lone \n in a binary-unsafe context */
      errno = EPROTO;
      return NULL;
    }
  }

  errno = EAGAIN;
  return NULL;
}

static int read_more(struct resp_window* w, struct resp_io* r) {
  ssize_t nr = resp_io_read(r, w->end, w->buf + RESP_WINDOW_SIZE - w->end);

  if(nr == -1)
    return -1;

  /* EOF */
  if(nr == 0) {
    errno = 0;
    return -1;
  }

  w->end += nr;

  assert(w->end <= w->buf + RESP_WINDOW_SIZE);

  return 0;
}

/* Returns a non-owning pointer. Caller must consume the token before the next
 * call to resp_token_read.
 */
const char* resp_token_read(struct resp_window* w, struct resp_io* r, size_t* len, int* is_huge) {
  *is_huge = 0;

  if(w->begin == w->end)
    w->begin = w->end = w->buf;

  while(1) {
    sanity_check(w);

    if(w->end == w->buf + RESP_WINDOW_SIZE) {
      if(w->begin > w->buf) {
        make_room(w);
        continue;
      } else {
        *is_huge = 1;
        return NULL;
      }
    }

    char* delim = find_delim(w->begin, w->end);

    if(delim != NULL) {
      const char* token = w->begin;
      *len = delim - w->begin;
      w->begin = delim + 2;
      return token;
    }

    if(errno != EAGAIN)
      return NULL;

    if(read_more(w, r) == -1)
      return NULL;
  }
}

/* Buffer must include space for the two-byte delimiter at the end */
int resp_token_read_block(struct resp_window* w, struct resp_io* r, char* buf, size_t len) {
  char *rbegin = buf, *rend = rbegin + len;

  size_t n = min(rend - rbegin, w->end - w->begin);
  memcpy(rbegin, w->begin, n);

  w->begin += n;
  rbegin += n;

  while(rbegin < rend) {
    n = rend - rbegin;
    ssize_t nr = resp_io_read(r, rbegin, n);

    if(nr == -1)
      return 1;

    rbegin += nr;
  }

  if(*(rend - 2) != '\r' || *(rend - 1) != '\n') {
    errno = EPROTO;
    return 1;
  }

  return 0;
}

static inline void* reindex(void* old_base, void* new_base, void* ptr) {
  return ptr - old_base + new_base;
}

char* resp_token_huge(struct resp_window* w, struct resp_io* r,
    char* buf, char* begin, size_t size, size_t skip, size_t* len) {
  assert(size > RESP_WINDOW_SIZE);

  /* At this point, the buffer contains all token, and no delimiter. Copy the
     buffer, then start growing it until we find the delimiter. */
  memcpy(begin, w->buf + skip, RESP_WINDOW_SIZE - skip);
  char *end = begin + RESP_WINDOW_SIZE - skip, *prev = end, *delim;

  do {
    ssize_t nr = resp_io_read(r, end, buf + size - end);

    if(nr == -1)
      goto error;

    /* EOF */
    if(nr == 0) {
      errno = 0;
      goto error;
    }

    end += nr;

    delim = find_delim(prev, end);

    if(delim != NULL)
      break;

    if(delim == NULL && errno != EAGAIN)
      goto error;

    if(end == buf + size) {
      size += RESP_WINDOW_SIZE;
      char* new_buf = realloc(buf, size);

      if(new_buf == NULL)
        goto error;

      begin = reindex(buf, new_buf, begin);
      end = reindex(buf, new_buf, end);
      buf = new_buf;
    }

    prev = end;
  } while(1);

  memcpy(w->buf, delim + 2, end - delim);
  w->begin = w->buf;
  w->end = w->buf + (end - delim - 2);

  *len = delim - begin;
  return realloc(buf, delim - buf);

  int err;
error:
  err = errno;
  free(buf);
  errno = err;
  return NULL;
}
