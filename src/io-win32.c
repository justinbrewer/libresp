/* SPDX-License-Identifier: LGPL-2.1-or-later */
#include <libresp.h>
#include <libresp-private.h>

#include <errno.h>
#include <stdlib.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <windows.h>

struct handle_io {
  struct resp_io base;
  HANDLE r, w;
};

struct socket_io {
  struct resp_io base;
  SOCKET r, w;
};

static ssize_t handle_read(struct resp_io* r, char* buf, size_t len) {
  struct handle_io* io = (struct handle_io*)r;
  DWORD bytes;

  if(ReadFile(io->r, (void*)buf, len, &bytes, NULL))
    return (ssize_t)bytes;

  /* TODO: Do some error mapping */
  errno = EIO;
  return -1;
}

static ssize_t handle_write(struct resp_io* r, const void* buf, size_t len) {
  struct handle_io* io = (struct handle_io*)r;
  DWORD bytes;

  if(WriteFile(io->w, buf, len, &bytes, NULL))
    return (ssize_t)bytes;

  /* TODO: Do some error mapping */
  errno = EIO;
  return -1;
}

static void handle_close(struct resp_io* r) {
  struct handle_io* io = (struct handle_io*)r;

  CloseHandle(io->r);

  if(io->w != io->r)
    CloseHandle(io->w);
}

static void socket_close(struct resp_io* r) {
  struct socket_io* io = (struct socket_io*)r;

  closesocket(io->r);

  if(io->w != io->r)
    closesocket(io->w);
}

static const struct resp_io_actions handle_actions = {
  .read = handle_read,
  .write = handle_write,
  .close = handle_close,
};

static const struct resp_io_actions socket_actions = {
  .read = handle_read,
  .write = handle_write,
  .close = socket_close,
};

struct resp_io* resp_io_tcp_init(const char* addr, const char* port) {
  struct addrinfo *result = NULL, *ptr = NULL, hints;

  ZeroMemory(&hints, sizeof(hints));
  hints.ai_family = AF_UNSPEC;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_protocol = IPPROTO_TCP;

  if(getaddrinfo(addr, port, &hints, &result) != 0) {
    /* TODO: Do some error mapping */
    errno = EIO;
    return NULL;
  }

  SOCKET s = INVALID_SOCKET;
  for(ptr = result; ptr != NULL; ptr = ptr->ai_next) {
    s = socket(ptr->ai_family, ptr->ai_socktype, ptr->ai_protocol);

    if(s == INVALID_SOCKET)
      continue;

    if(!connect(s, ptr->ai_addr, (int)ptr->ai_addrlen) != 0)
      break;

    closesocket(s);
    s = INVALID_SOCKET;
  }

  if(result != NULL)
    freeaddrinfo(result);

  if(s == INVALID_SOCKET) {
    /* TODO: Do some error mapping */
    errno = EIO;
    return NULL;
  }

  return resp_io_socket_init(s, s);
}

struct resp_io* resp_io_socket_init(SOCKET r, SOCKET w) {
  struct socket_io* io = calloc(1, sizeof(struct socket_io));

  if(io == NULL)
    return NULL;

  io->base.actions = &socket_actions;
  io->r = r;
  io->w = w;

  return (struct resp_io*)io;
}

struct resp_io* resp_io_fd_init(HANDLE r, HANDLE w) {
  struct handle_io* io = calloc(1, sizeof(struct handle_io));

  if(io == NULL)
    return NULL;

  io->base.actions = &handle_actions;
  io->r = r;
  io->w = w;

  return (struct resp_io*)io;
}

void resp_io_get_socket(struct resp_io* r, SOCKET* fd) {
  struct socket_io* io = (struct socket_io*)r;

  fd[0] = io->r;
  fd[1] = io->w;
}

void resp_io_get_fd(struct resp_io* r, HANDLE* fd) {
  struct handle_io* io = (struct handle_io*)r;

  fd[0] = io->r;
  fd[1] = io->w;
}
