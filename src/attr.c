/* SPDX-License-Identifier: LGPL-2.1-or-later */
#include <libresp.h>
#include <libresp-private.h>

#include <stdlib.h>
#include <string.h>

const struct resp_attr resp_default_attr = {
  .flags = resp_attr_blocking | resp_attr_readthrd,
};

/** Create a resp_attr object with default values
 *
 * By default:
 *   blocking is enabled
 *   timeout is disabled
 *   readthrd is enabled
 */
struct resp_attr* resp_attr_init() {
  struct resp_attr* attr = calloc(1, sizeof(struct resp_attr));

  if(!attr) {
    return NULL;
  }

  memcpy(attr, &resp_default_attr, sizeof(struct resp_attr));
  return attr;
}

/** Free a resp_attr object.
 *
 * NULL is a valid noop.
 */
void resp_attr_free(struct resp_attr* attr) {
  free(attr);
}

/** Sets blocking mode to the specified value.
 *
 * If `b` is true, calls to resp_read will block if a message is not available.
 * If `b` is false, calls to resp_read will not block.
 */
void resp_attr_set_blocking(struct resp_attr* attr, bool b) {
  if(b) {
    attr->flags |= resp_attr_blocking;
  } else {
    attr->flags &= ~resp_attr_blocking;
  }
}

/** Sets timeout mode to the specified value
 *
 * If `ts` is non-NULL, timeout mode is enabled, and calls to resp_read will
 * timeout after the specified interval. If `ts` is NULL, timeout mode is
 * disabled, and calls to resp_read will block until a message is available.
 * This setting has no effect if blocking mode or the read thread are disabled.
 */
void resp_attr_set_timeout(struct resp_attr* attr, const struct timespec* ts) {
  if(ts) {
    attr->flags |= resp_attr_timeout;
    memcpy(&attr->timeout, ts, sizeof(struct timespec));
  } else {
    attr->flags &= ~resp_attr_timeout;
  }
}

/** Enables/disables the message read thread
 *
 * If this option is enabled, a thread will be created that parses the inbound
 * data and enqueues messages for resp_read to fetch.
 *
 * If this option is disabled, then message parsing will be performed directly
 * in resp_read. 
 *
 * If this option is disabled, then blocking mode cannot be disabled.
 * Attempting to initialize a context with both options disabled will result in
 * an error.
 */
void resp_attr_set_readthrd(struct resp_attr* attr, bool b) {
  if(b) {
    attr->flags |= resp_attr_readthrd;
  } else {
    attr->flags &= ~resp_attr_readthrd;
  }
}
