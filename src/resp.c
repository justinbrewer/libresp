/* SPDX-License-Identifier: LGPL-2.1-or-later */
#include <libresp.h>
#include <libresp-private.h>

#include <assert.h>
#include <pthread.h>
#include <stdatomic.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define RESP_MSG_ERR ((void*)-1)

static void timespec_add(struct timespec* l, const struct timespec* r) {
  l->tv_sec += r->tv_sec;
  l->tv_nsec += r->tv_nsec;

  l->tv_sec += l->tv_nsec / 1000000000;
  l->tv_nsec %= 1000000000;
}

static void* resp_thrd_run(void* arg) {
  struct resp_ctx* ctx = arg;
  struct resp_msg* msg;
  struct resp_window window = { .begin = NULL, .end = NULL };

  while(1) {
    msg = resp_msg_read(&window, ctx->io);

    if(msg == NULL) {
      atomic_store_explicit(&ctx->error, errno, memory_order_release);
      queue_push(&ctx->queue, RESP_MSG_ERR);
      return NULL;
    }

    queue_push(&ctx->queue, msg);
  }

  return NULL;
}

/** Create a resp context
 *
 * `io` must be non-NULL.
 * If `attr` is NULL, defaults will be used.
 *
 * \see resp_attr_init
 */
struct resp_ctx* resp_init(struct resp_io* io, const struct resp_attr* attr) {
  int err;
  struct resp_ctx* ctx;

  if(!io) {
    errno = EINVAL;
    return NULL;
  }

  attr = attr ? attr : &resp_default_attr;

  if(!(attr->flags & (resp_attr_blocking | resp_attr_readthrd))) {
    errno = EINVAL;
    return NULL;
  }

  ctx = calloc(1, sizeof(struct resp_ctx)
      + ((attr->flags & resp_attr_readthrd) ? 0 : sizeof(struct resp_window)));

  if(!ctx) {
    return NULL;
  }

  memcpy(&ctx->attr, attr, sizeof(struct resp_attr));
  ctx->io = io;

  if(ctx->attr.flags & resp_attr_readthrd) {
    queue_init(&ctx->queue);

    if(pthread_create(&ctx->thread, NULL, resp_thrd_run, ctx)) {
      queue_destroy(&ctx->queue);
      free(ctx);
      return NULL;
    }
  } else {
    if((err = pthread_mutex_init(&ctx->read_mutex, NULL))) {
      free(ctx);
      errno = err;
      return NULL;
    }

    ctx->read_window = (struct resp_window*)(ctx + 1);
  }

  return ctx;
}

/** Free a resp context
 */
void resp_free(struct resp_ctx* ctx) {
  if(ctx->attr.flags & resp_attr_readthrd) {
    pthread_cancel(ctx->thread);
    pthread_join(ctx->thread, NULL);

    struct resp_msg* msg = queue_trypop(&ctx->queue);
    if(msg != RESP_MSG_ERR) {
      resp_msg_free(msg);
    }

    queue_destroy(&ctx->queue);
  } else {
    pthread_mutex_destroy(&ctx->read_mutex);
  }

  resp_io_free(ctx->io);
  free(ctx);
}

/** Returns the next message from the stream
 *
 * Returns NULL if an error occurred and sets errno. After an error occurs,
 * no further messages be read on this context.
 *
 * Errors:
 *   EPROTO - A malformed message arrived
 *   0 - End-of-file detected
 */
struct resp_msg* resp_read(struct resp_ctx* ctx) {
  if(ctx->error != 0) {
    errno = ctx->error;
    return NULL;
  }

  struct resp_msg* msg;


  if(ctx->attr.flags & resp_attr_readthrd) {
    if(ctx->attr.flags & resp_attr_blocking) {
      if(ctx->attr.flags & resp_attr_timeout) {
        struct timespec t;
        clock_gettime(CLOCK_MONOTONIC, &t);
        timespec_add(&t, &ctx->attr.timeout);
        msg = queue_timedpop(&ctx->queue, &t);
      } else {
        msg = queue_pop(&ctx->queue);
      }
    } else {
      msg = queue_trypop(&ctx->queue);
      errno = (errno == EBUSY) ? EAGAIN : errno;
    }

    if(msg == RESP_MSG_ERR) {
      errno = atomic_load_explicit(&ctx->error, memory_order_acquire);
      return NULL;
    }
  } else {
    int err;
    pthread_mutex_lock(&ctx->read_mutex);
    msg = resp_msg_read(ctx->read_window, ctx->io);
    err = errno;
    pthread_mutex_unlock(&ctx->read_mutex);
    errno = err;
  }

  return msg;
}
