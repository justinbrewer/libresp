/* SPDX-License-Identifier: LGPL-2.1-or-later */
#ifndef __QUEUE_H__
#define __QUEUE_H__

#include <compiler.h>

struct queue;
struct timespec;

static int queue_init(struct queue* q) UNUSED;
static void queue_destroy(struct queue* q) UNUSED;

static void* queue_pop(struct queue* q) UNUSED;
static void* queue_trypop(struct queue* q) UNUSED;
static void* queue_timedpop(struct queue* q, const struct timespec* abs_timeout) UNUSED;

static int queue_push(struct queue* q, void* v) UNUSED;
static int queue_trypush(struct queue* q, void* v) UNUSED;
static int queue_timedpush(struct queue* q, void* v, const struct timespec* abs_timeout) UNUSED;

#if defined(RESP_HOST_POSIX) || defined(RESP_HOST_WIN32)
#include <queue-posix.h>
#else
#error "Platform support not implemented"
#endif

#endif
