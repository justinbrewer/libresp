/* SPDX-License-Identifier: LGPL-2.1-or-later */
#ifndef __LIBRESP_H__
#define __LIBRESP_H__

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <sys/types.h>

struct timespec;

#ifdef __cplusplus__
extern "C" {
#endif

#if defined(__unix__)
typedef int resp_fd;
typedef int resp_socket;
#elif defined(_WIN32)
/* Oof, 5k+ lines for a single void* typedef
#include <winnt.h>
*/
typedef void* HANDLE;
typedef HANDLE resp_fd;
typedef uintptr_t SOCKET;
typedef SOCKET resp_socket;
#else
#error FIXME
#endif

struct resp_ctx;
struct resp_attr;

enum resp_type {
  resp_type_null = 0,
  resp_type_simple_string,
  resp_type_error,
  resp_type_integer,
  resp_type_bulk_string,
  resp_type_array,
  resp_type_inline_command,
};

typedef int64_t resp_int;

struct resp_msg {
  enum resp_type type;

  union {
    /* type == null */
    enum resp_type null_type;

    /* type == integer */
    resp_int integer;

    /* type == simple_string, error, bulk_string, inline_command */
    struct {
      size_t length;
      char string[];
    };

    /* type == array */
    struct {
      size_t num;
      struct resp_msg* array[];
    };
  };
};

struct resp_io;

struct resp_io_actions {
  ssize_t (*read)(struct resp_io* r, char* buf, size_t len);
  ssize_t (*write)(struct resp_io* r, const void* buf, size_t len);
  void (*close)(struct resp_io*);
};

struct resp_io {
  const struct resp_io_actions* actions;
};

struct resp_attr* resp_attr_init();
void resp_attr_free(struct resp_attr*);
void resp_attr_set_blocking(struct resp_attr*, bool);
void resp_attr_set_timeout(struct resp_attr*, const struct timespec*);
void resp_attr_set_readthrd(struct resp_attr*, bool);

struct resp_io* resp_io_tcp_init(const char* addr, const char* port);
struct resp_io* resp_io_socket_init(resp_socket read, resp_socket write);
struct resp_io* resp_io_fd_init(resp_fd read, resp_fd write);
void resp_io_free(struct resp_io*);

struct resp_ctx* resp_init(struct resp_io*, const struct resp_attr*);
void resp_free(struct resp_ctx*);

struct resp_msg* resp_read(struct resp_ctx*);
int resp_write(struct resp_ctx*, const struct resp_msg*);
int resp_writef(struct resp_ctx*, const char* fmt, ...);
void resp_msg_free(struct resp_msg*);

#ifdef __cplusplus__
}
#endif

#endif
