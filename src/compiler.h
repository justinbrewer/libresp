/* SPDX-License-Identifier: LGPL-2.1-or-later */
#ifndef __COMPILER_H__
#define __COMPILER_H__

#if defined(__GNUC__)
#define UNUSED __attribute__((unused))
#else
#define UNUSED
#endif

#define min(l,r) \
  ({ __typeof__(l) _l = (l); __typeof__(r) _r = (r); _l < _r ? _l : _r; })

#if defined(HAVE_SCHED_H)
#include <sched.h>
#else
static inline void sched_yield() {}
#endif

#endif
