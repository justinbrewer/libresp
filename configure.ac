AC_PREREQ([2.69])
AC_INIT([libresp], [dev], [Justin Brewer <jzb0012@auburn.edu>])
AC_CONFIG_MACRO_DIR([m4])
AM_INIT_AUTOMAKE([-Wall -Werror foreign subdir-objects])
PKG_PROG_PKG_CONFIG([0.27])
PKG_INSTALLDIR

AC_CANONICAL_HOST
AC_CANONICAL_BUILD

posix_host=no
win32_host=no
case ${host_os} in
    mingw*|msys*)
        export ac_cv_func_malloc_0_nonnull=yes
        export ac_cv_func_realloc_0_nonnull=yes
        win32_host=yes
        ;;
    *)
        posix_host=yes
        ;;
esac

# Options
AC_ARG_ENABLE([fuzzer],
    AS_HELP_STRING([--enable-fuzzer],[Enables building of fuzzer utilities. Requires -fsanitize=fuzzer support.]))
AM_CONDITIONAL([FUZZER], [test "x$enable_fuzzer" = "xyes"])

# Workaround for -fsanitize=address rpl_malloc issue
if test "x$enable_fuzzer" = "xyes"; then
    export ac_cv_func_malloc_0_nonnull=yes
fi

# Checks
AC_CONFIG_SRCDIR([src/libresp.h])
AC_CONFIG_SRCDIR([src/resp.c])

AM_PROG_AR
AC_PROG_CC
LT_INIT([win32-dll])

if test "x$enable_fuzzer" = "xyes"; then
    AC_MSG_CHECKING([if $CC supports -fsanitize=fuzzer])
    AC_LANG_PUSH([C])
    ac_saved_cflags="$CFLAGS"
    CFLAGS="-fsanitize=fuzzer"
    AC_COMPILE_IFELSE([AC_LANG_PROGRAM([])],
    [AC_MSG_RESULT([yes])],
    [AC_MSG_RESULT([no]);AC_MSG_ERROR([compiler does not support -fsanitize=fuzzer])])
    CFLAGS="$ac_saved_cflags"
    AC_LANG_POP([C])
fi

AX_CODE_COVERAGE

AC_CHECK_HEADERS([fcntl.h limits.h stddef.h sched.h])
AC_TYPE_INT64_T
AC_TYPE_SIZE_T
AC_TYPE_UINT8_T
AC_FUNC_MALLOC
AC_FUNC_MMAP

PKG_CHECK_MODULES([CHECK],[check >= 0.12],[have_check=yes],[have_check=no])
AM_CONDITIONAL([HAVE_CHECK],[test "$have_check" = "yes"])

dnl ax_valgrind_check.m4 uses GNU make features
if test "$have_check" = "yes"; then
    case ${build_os} in
        linux*)
            AX_VALGRIND_DFLT([sgcheck], [off])
            AX_VALGRIND_DFLT([helgrind], [off])
            AX_VALGRIND_DFLT([drd], [off])
            AX_VALGRIND_CHECK
            ;;
        *)
            AC_SUBST([VALGRIND_CHECK_RULES],[])
            AM_CONDITIONAL([VALGRIND_ENABLED],[test "1" = "2"])
            ;;
    esac
else
    AC_SUBST([VALGRIND_CHECK_RULES],[])
    AM_CONDITIONAL([VALGRIND_ENABLED],[test "1" = "2"])
fi

AM_CONDITIONAL([POSIX_HOST],[test "$posix_host" = "yes"])
AM_CONDITIONAL([WIN32_HOST],[test "$win32_host" = "yes"])

AC_CONFIG_FILES([Makefile libresp.pc])
AC_OUTPUT
