#include <libresp.h>
#include <libresp-private.h>
#include <unit-utils.h>

#include <check.h>
#include <errno.h>
#include <stdlib.h>
#include <time.h>

START_TEST(modes_nonblocking) {
  struct resp_attr* attr = resp_attr_init();
  ck_assert_ptr_ne(attr, NULL);
  resp_attr_set_blocking(attr, 0);
  loopback_setup_attr(attr);
  resp_attr_free(attr);

  struct resp_msg* msg = resp_read(ctx);
  ck_assert_ptr_eq(msg, NULL);
  ck_assert_int_eq(errno, EAGAIN);
} END_TEST

START_TEST(modes_timeout) {
  const struct timespec t = { .tv_sec = 0, .tv_nsec = 10000000 };

  struct resp_attr* attr = resp_attr_init();
  ck_assert_ptr_ne(attr, NULL);
  resp_attr_set_timeout(attr, &t);
  loopback_setup_attr(attr);
  resp_attr_free(attr);

  struct resp_msg* msg = resp_read(ctx);
  ck_assert_ptr_eq(msg, NULL);
  ck_assert_int_eq(errno, ETIMEDOUT);
} END_TEST

Suite* mk_suite() {
  TCase* t;
  Suite* s = suite_create("resp");

  t = tcase_create("modes");
  tcase_add_checked_fixture(t, NULL, loopback_teardown);
  tcase_add_test(t, modes_nonblocking);
  tcase_add_test(t, modes_timeout);
  suite_add_tcase(s,t);

  return s;
}

int main() {
  SRunner* r = srunner_create(mk_suite());
  srunner_run_all(r, CK_NORMAL);
  int f = srunner_ntests_failed(r);
  srunner_free(r);
  return f ? EXIT_FAILURE : EXIT_SUCCESS;
}
