#include <queue.h>

#include <check.h>
#include <errno.h>
#include <stdlib.h>
#include <time.h>

#define TOKEN ((void*)1)

static struct queue q;

static void queue_setup() {
  queue_init(&q);
}

static void queue_teardown() {
  queue_destroy(&q);
}

START_TEST(push_trypush) {
  int r = queue_trypush(&q, TOKEN);
  ck_assert_int_eq(r, 0);
  r = queue_trypush(&q, TOKEN);
  ck_assert_int_eq(r, EBUSY);
} END_TEST

START_TEST(push_timedpush) {
  struct timespec t;
  clock_gettime(CLOCK_MONOTONIC, &t);
  t.tv_nsec += 10000000;
  t.tv_sec += t.tv_nsec / 1000000000;
  t.tv_nsec %= 1000000000;

  int r = queue_timedpush(&q, TOKEN, &t);
  ck_assert_int_eq(r, 0);
  r = queue_timedpush(&q, TOKEN, &t);
  ck_assert_int_eq(r, ETIMEDOUT);
} END_TEST

void* push_deadlock_run(void* ptr) {
  (void)ptr;

  queue_push(&q, TOKEN);
  queue_push(&q, TOKEN);
  queue_push(&q, TOKEN);

  return NULL;
}

START_TEST(push_deadlock) {
  pthread_t th;
  int r = pthread_create(&th, NULL, push_deadlock_run, NULL);
  ck_assert_int_eq(r, 0);

  const struct timespec t = { .tv_sec = 0, .tv_nsec = 10000000 };
  clock_nanosleep(CLOCK_MONOTONIC, 0, &t, NULL);
  queue_pop(&q);
  clock_nanosleep(CLOCK_MONOTONIC, 0, &t, NULL);
  queue_pop(&q);
  clock_nanosleep(CLOCK_MONOTONIC, 0, &t, NULL);
  queue_pop(&q);

  pthread_join(th, NULL);
} END_TEST

START_TEST(pop_trypop) {
  int r = queue_trypush(&q, TOKEN);
  ck_assert_int_eq(r, 0);

  void* v = queue_trypop(&q);
  ck_assert_ptr_eq(v, TOKEN);
  v = queue_trypop(&q);
  ck_assert_ptr_eq(v, NULL);
  ck_assert_int_eq(errno, EBUSY);
} END_TEST

START_TEST(pop_timedpop) {
  int r = queue_trypush(&q, TOKEN);
  ck_assert_int_eq(r, 0);

  struct timespec t;
  clock_gettime(CLOCK_MONOTONIC, &t);
  t.tv_nsec += 10000000;
  t.tv_sec += t.tv_nsec / 1000000000;
  t.tv_nsec %= 1000000000;

  void* v = queue_timedpop(&q, &t);
  ck_assert_ptr_eq(v, TOKEN);
  v = queue_timedpop(&q, &t);
  ck_assert_ptr_eq(v, NULL);
  ck_assert_int_eq(errno, ETIMEDOUT);
} END_TEST

Suite* mk_suite() {
  TCase* t;
  Suite* s = suite_create("queue");

  t = tcase_create("push");
  tcase_add_checked_fixture(t, queue_setup, queue_teardown);
  tcase_add_test(t, push_trypush);
  tcase_add_test(t, push_timedpush);
  tcase_add_test(t, push_deadlock);
  suite_add_tcase(s,t);

  t = tcase_create("pop");
  tcase_add_checked_fixture(t, queue_setup, queue_teardown);
  tcase_add_test(t, pop_trypop);
  tcase_add_test(t, pop_timedpop);
  suite_add_tcase(s,t);

  return s;
}

int main() {
  SRunner* r = srunner_create(mk_suite());
  srunner_run_all(r, CK_NORMAL);
  int f = srunner_ntests_failed(r);
  srunner_free(r);
  return f ? EXIT_FAILURE : EXIT_SUCCESS;
}
