#include <libresp.h>
#include <stdio.h>

#if defined(_WIN32)
#  include <inttypes.h>
#  if defined(_WIN64)
#    define PRI_SIZET PRIu64
#  else
#    define PRI_SIZET PRIu32
#  endif
#else
#  define PRI_SIZET "zu"
#endif

int main(int argc, char** argv) {
  if(argc != 3) {
    fprintf(stderr, "usage: %s <ip> <port>\n", argv[0]);
    return 1;
  }

  struct resp_io* io = resp_io_tcp_init(argv[1], argv[2]);

  if(io == NULL) {
    perror("resp_io_tcp_init");
    return 1;
  }

  struct resp_ctx* ctx = resp_init(io, NULL);

  if(ctx == NULL) {
    perror("resp_init");
    resp_io_free(io);
    return 1;
  }

  { const char buf[] = "PING\r\n";
    io->actions->write(io, buf, sizeof(buf));
  }

  struct resp_msg* reply = resp_read(ctx);

  if(reply == NULL) {
    perror("resp_read");
    resp_free(ctx);
    return 1;
  }

  printf("%d %"PRI_SIZET" %s\n", reply->type, reply->length, reply->string);

  resp_free(ctx);
  return 0;
}
