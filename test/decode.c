#include <libresp.h>
#include <libresp-private.h>
#include <unit-utils.h>

#include <check.h>
#include <errno.h>
#include <limits.h>
#include <stdlib.h>

#define PAYLOAD_ERROR(_payload, _err) \
  ({ resp_io_write(io, _payload, sizeof(_payload)); \
     struct resp_msg* msg = resp_read(ctx); \
     ck_assert_ptr_eq(msg, NULL); \
     ck_assert_int_eq(errno, _err); 0; })


START_TEST(types_null_a) {
  const char payload[] = "$-1\r\n";
  resp_io_write(io, payload, sizeof(payload));

  struct resp_msg* msg = resp_read(ctx);
  ck_assert_ptr_ne(msg, NULL);
  ck_assert_int_eq(msg->type, resp_type_null);
  resp_msg_free(msg);
} END_TEST

START_TEST(types_null_b) {
  const char payload[] = "*-1\r\n";
  resp_io_write(io, payload, sizeof(payload));

  struct resp_msg* msg = resp_read(ctx);
  ck_assert_ptr_ne(msg, NULL);
  ck_assert_int_eq(msg->type, resp_type_null);
  resp_msg_free(msg);
} END_TEST

START_TEST(types_simple_string) {
  const char payload[] = "+foo\r\n";
  resp_io_write(io, payload, sizeof(payload));

  struct resp_msg* msg = resp_read(ctx);
  ck_assert_ptr_ne(msg, NULL);
  ck_assert_int_eq(msg->type, resp_type_simple_string);
  ck_assert_int_eq(msg->length, 3);
  ck_assert_str_eq(msg->string, "foo");
  resp_msg_free(msg);
} END_TEST

START_TEST(types_error) {
  const char payload[] = "-foo\r\n";
  resp_io_write(io, payload, sizeof(payload));

  struct resp_msg* msg = resp_read(ctx);
  ck_assert_ptr_ne(msg, NULL);
  ck_assert_int_eq(msg->type, resp_type_error);
  ck_assert_int_eq(msg->length, 3);
  ck_assert_str_eq(msg->string, "foo");
  resp_msg_free(msg);
} END_TEST

START_TEST(types_int) {
  const char payload[] = ":1234\r\n";
  resp_io_write(io, payload, sizeof(payload));

  struct resp_msg* msg = resp_read(ctx);
  ck_assert_ptr_ne(msg, NULL);
  ck_assert_int_eq(msg->type, resp_type_integer);
  ck_assert_int_eq(msg->integer, 1234);
  resp_msg_free(msg);
} END_TEST

START_TEST(types_inline_command) {
  const char payload[] = "BAR\r\n";
  resp_io_write(io, payload, sizeof(payload));

  struct resp_msg* msg = resp_read(ctx);
  ck_assert_ptr_ne(msg, NULL);
  ck_assert_int_eq(msg->type, resp_type_inline_command);
  ck_assert_int_eq(msg->length, 3);
  ck_assert_str_eq(msg->string, "BAR");
  resp_msg_free(msg);
} END_TEST

START_TEST(types_bulk_string) {
  const char payload[] = "$5\r\n\r\0\n\r\n\r\n";
  resp_io_write(io, payload, sizeof(payload));

  struct resp_msg* msg = resp_read(ctx);
  ck_assert_ptr_ne(msg, NULL);
  ck_assert_int_eq(msg->type, resp_type_bulk_string);
  ck_assert_int_eq(msg->length, 5);
  ck_assert_mem_eq(msg->string, "\r\0\n\r\n", 5);
  resp_msg_free(msg);
} END_TEST

START_TEST(types_array) {
  const char payload[] = "*2\r\n+foo\r\n+bar\r\n";
  resp_io_write(io, payload, sizeof(payload));

  struct resp_msg* msg = resp_read(ctx);
  ck_assert_ptr_ne(msg, NULL);
  ck_assert_int_eq(msg->type, resp_type_array);
  ck_assert_int_eq(msg->num, 2);
  ck_assert_int_eq(msg->array[0]->type, resp_type_simple_string);
  ck_assert_int_eq(msg->array[0]->length, 3);
  ck_assert_str_eq(msg->array[0]->string, "foo");
  ck_assert_int_eq(msg->array[1]->type, resp_type_simple_string);
  ck_assert_int_eq(msg->array[1]->length, 3);
  ck_assert_str_eq(msg->array[1]->string, "bar");
  resp_msg_free(msg);
} END_TEST

START_TEST(types_invalid) {
  PAYLOAD_ERROR("\xFF\r\n", EPROTO);
} END_TEST

START_TEST(types_empty) {
  PAYLOAD_ERROR("\r\n", EPROTO);
} END_TEST

START_TEST(limits_int_max) {
  const char payload[] = ":9223372036854775807\r\n";
  resp_io_write(io, payload, sizeof(payload));

  struct resp_msg* msg = resp_read(ctx);
  ck_assert_ptr_ne(msg, NULL);
  ck_assert_int_eq(msg->type, resp_type_integer);
  ck_assert_int_eq(msg->integer, LLONG_MAX);
  resp_msg_free(msg);
} END_TEST

START_TEST(limits_int_max_overflow) {
  PAYLOAD_ERROR(":9223372036854775808\r\n", EPROTO);
} END_TEST

START_TEST(limits_int_min) {
  const char payload[] = ":-9223372036854775808\r\n";
  resp_io_write(io, payload, sizeof(payload));

  struct resp_msg* msg = resp_read(ctx);
  ck_assert_ptr_ne(msg, NULL);
  ck_assert_int_eq(msg->type, resp_type_integer);
  ck_assert_int_eq(msg->integer, LLONG_MIN);
  resp_msg_free(msg);
} END_TEST

START_TEST(limits_int_min_overflow) {
  PAYLOAD_ERROR(":-9223372036854775809\r\n", EPROTO);
} END_TEST

START_TEST(limits_bulk_string_overflow) {
  PAYLOAD_ERROR("$9223372036854775808\r\n", EPROTO);
} END_TEST

START_TEST(limits_bulk_string_max) {
  PAYLOAD_ERROR("$536870913\r\n", EPROTO);
} END_TEST

START_TEST(limits_bulk_string_min) {
  PAYLOAD_ERROR("$-2\r\n", EPROTO);
} END_TEST

START_TEST(limits_array_overflow) {
  PAYLOAD_ERROR("*9223372036854775808\r\n", EPROTO);
} END_TEST

START_TEST(limits_array_min) {
  PAYLOAD_ERROR("*-2\r\n", EPROTO);
} END_TEST

START_TEST(token_cr) {
  PAYLOAD_ERROR("+foo\rbar\r\n", EPROTO);
} END_TEST

START_TEST(token_lf) {
  PAYLOAD_ERROR("+foo\nbar\r\n", EPROTO);
} END_TEST

START_TEST(token_bulk_crlf) {
  PAYLOAD_ERROR("$3\r\nfoobar\r\n", EPROTO);
} END_TEST

START_TEST(token_split) {
  const char payload_a[] = "+foo\r\n+b";
  resp_io_write(io, payload_a, sizeof(payload_a) - 1);

  struct resp_msg* msg = resp_read(ctx);
  ck_assert_ptr_ne(msg, NULL);
  ck_assert_int_eq(msg->type, resp_type_simple_string);
  ck_assert_int_eq(msg->length, 3);
  ck_assert_str_eq(msg->string, "foo");
  resp_msg_free(msg);

  const char payload_b[] = "ar\r\n+baz\r";
  resp_io_write(io, payload_b, sizeof(payload_b) - 1);

  msg = resp_read(ctx);
  ck_assert_ptr_ne(msg, NULL);
  ck_assert_int_eq(msg->type, resp_type_simple_string);
  ck_assert_int_eq(msg->length, 3);
  ck_assert_str_eq(msg->string, "bar");
  resp_msg_free(msg);

  resp_io_write(io, "\n", 1);

  msg = resp_read(ctx);
  ck_assert_ptr_ne(msg, NULL);
  ck_assert_int_eq(msg->type, resp_type_simple_string);
  ck_assert_int_eq(msg->length, 3);
  ck_assert_str_eq(msg->string, "baz");
  resp_msg_free(msg);
} END_TEST

START_TEST(token_make_room) {
  /* Prevent window from resetting by not concluding any writes on a crlf */
  const char payload[] = "0123456789ABCDEF\r\n+";
  const size_t n = (RESP_WINDOW_SIZE / (sizeof(payload) - 1)) + 2;

  resp_io_write(io, "+", 1);
  for(size_t i = 0; i < n; i++) {
    resp_io_write(io, payload, sizeof(payload) - 1);

    struct resp_msg* msg = resp_read(ctx);
    ck_assert_ptr_ne(msg, NULL);
    ck_assert_int_eq(msg->type, resp_type_simple_string);
    ck_assert_int_eq(msg->length, 16);
    ck_assert_str_eq(msg->string, "0123456789ABCDEF");
    resp_msg_free(msg);
  }
} END_TEST

START_TEST(token_huge_string) {
  const char payload[] = "+123456789ABCDEF";
  const size_t n = 2 * (RESP_WINDOW_SIZE / (sizeof(payload) - 1)) + 2;

  for(size_t i = 0; i < n; i++) {
    resp_io_write(io, payload, sizeof(payload) - 1);
  }
  resp_io_write(io, "\r\n", 2);

  struct resp_msg* msg = resp_read(ctx);
  ck_assert_ptr_ne(msg, NULL);
  ck_assert_int_eq(msg->type, resp_type_simple_string);
  ck_assert_int_eq(msg->length, n * (sizeof(payload) - 1) - 1);

  ck_assert_mem_eq(msg->string, payload + 1, sizeof(payload) - 2);
  for(size_t i = 1; i < n; i++) {
    ck_assert_mem_eq(msg->string + (i * (sizeof(payload) - 1)) - 1, payload, sizeof(payload) - 1);
  }

  resp_msg_free(msg);
} END_TEST

START_TEST(token_huge_error) {
  const char payload[] = "-123456789ABCDEF";
  const size_t n = 2 * (RESP_WINDOW_SIZE / (sizeof(payload) - 1)) + 2;

  for(size_t i = 0; i < n; i++) {
    resp_io_write(io, payload, sizeof(payload) - 1);
  }
  resp_io_write(io, "\r\n", 2);

  struct resp_msg* msg = resp_read(ctx);
  ck_assert_ptr_ne(msg, NULL);
  ck_assert_int_eq(msg->type, resp_type_error);
  ck_assert_int_eq(msg->length, n * (sizeof(payload) - 1) - 1);

  ck_assert_mem_eq(msg->string, payload + 1, sizeof(payload) - 2);
  for(size_t i = 1; i < n; i++) {
    ck_assert_mem_eq(msg->string + (i * (sizeof(payload) - 1)) - 1, payload, sizeof(payload) - 1);
  }

  resp_msg_free(msg);
} END_TEST

START_TEST(token_huge_int) {
  const char payload[] = "1234567890123456";
  const size_t n = 2 * (RESP_WINDOW_SIZE / (sizeof(payload) - 1)) + 2;

  resp_io_write(io, ":", 1);
  for(size_t i = 0; i < n; i++) {
    resp_io_write(io, payload, sizeof(payload) - 1);
  }
  resp_io_write(io, "\r\n", 2);

  struct resp_msg* msg = resp_read(ctx);
  ck_assert_ptr_eq(msg, NULL);
  ck_assert_int_eq(errno, EPROTO);
} END_TEST

START_TEST(token_huge_inline) {
  const char payload[] = "ABCDEF1234567890";
  const size_t n = 2 * (RESP_WINDOW_SIZE / (sizeof(payload) - 1)) + 2;

  for(size_t i = 0; i < n; i++) {
    resp_io_write(io, payload, sizeof(payload) - 1);
  }
  resp_io_write(io, "\r\n", 2);

  struct resp_msg* msg = resp_read(ctx);
  ck_assert_ptr_ne(msg, NULL);
  ck_assert_int_eq(msg->type, resp_type_inline_command);
  ck_assert_int_eq(msg->length, n * (sizeof(payload) - 1));

  for(size_t i = 0; i < n; i++) {
    ck_assert_mem_eq(msg->string + (i * (sizeof(payload) - 1)), payload, sizeof(payload) - 1);
  }

  resp_msg_free(msg);
} END_TEST

START_TEST(token_huge_invalid) {
  const char payload[] = "1234567890123456";
  const size_t n = 2 * (RESP_WINDOW_SIZE / (sizeof(payload) - 1)) + 2;

  resp_io_write(io, "\xFF", 1);
  for(size_t i = 0; i < n; i++) {
    resp_io_write(io, payload, sizeof(payload) - 1);
  }
  resp_io_write(io, "\r\n", 2);

  struct resp_msg* msg = resp_read(ctx);
  ck_assert_ptr_eq(msg, NULL);
  ck_assert_int_eq(errno, EPROTO);
} END_TEST

START_TEST(token_split_block) {
  const char payload_a[] = "$-1\r\n$16\r\n01234567";
  resp_io_write(io, payload_a, sizeof(payload_a) - 1);

  struct resp_msg* msg = resp_read(ctx);
  ck_assert_ptr_ne(msg, NULL);
  ck_assert_int_eq(msg->type, resp_type_null);
  resp_msg_free(msg);

  const char payload_b[] = "89ABCDEF\r\n";
  resp_io_write(io, payload_b, sizeof(payload_b) - 1);

  msg = resp_read(ctx);
  ck_assert_ptr_ne(msg, NULL);
  ck_assert_int_eq(msg->type, resp_type_bulk_string);
  ck_assert_int_eq(msg->length, 16);
  ck_assert_mem_eq(msg->string, "0123456789ABCDEF", 16);
  resp_msg_free(msg);
} END_TEST

START_TEST(token_huge_next) {
  const char payload[] = "+123456789ABCDEF";
  const size_t n = 2 * (RESP_WINDOW_SIZE / (sizeof(payload) - 1)) + 2;

  for(size_t i = 0; i < n; i++) {
    resp_io_write(io, payload, sizeof(payload) - 1);
  }
  resp_io_write(io, "\r\n-foobar\r\n", 11);

  struct resp_msg* msg = resp_read(ctx);
  ck_assert_ptr_ne(msg, NULL);
  ck_assert_int_eq(msg->type, resp_type_simple_string);
  resp_msg_free(msg);

  msg = resp_read(ctx);
  ck_assert_ptr_ne(msg, NULL);
  ck_assert_int_eq(msg->type, resp_type_error);
  ck_assert_int_eq(msg->length, 6);
  ck_assert_str_eq(msg->string, "foobar");
  resp_msg_free(msg);
} END_TEST

START_TEST(io_fail) {
  loopback_io_force_eof(io);

  struct resp_msg* msg = resp_read(ctx);
  ck_assert_ptr_eq(msg, NULL);
  ck_assert_int_eq(errno, 0);
} END_TEST

START_TEST(io_fail_huge) {
  const char payload[] = "+123456789ABCDEF";
  const size_t n = 2 * (RESP_WINDOW_SIZE / (sizeof(payload) - 1)) + 2;

  for(size_t i = 0; i < n; i++) {
    resp_io_write(io, payload, sizeof(payload) - 1);
  }

  loopback_io_force_eof(io);

  struct resp_msg* msg = resp_read(ctx);
  ck_assert_ptr_eq(msg, NULL);
  ck_assert_int_eq(errno, 0);
} END_TEST

START_TEST(misc_array_deep_error) {
  PAYLOAD_ERROR("*2\r\n:1\r\n*1\r\n$-2\r\n", EPROTO);
} END_TEST

Suite* mk_suite() {
  TCase* t;
  Suite* s = suite_create("decode");

  t = tcase_create("types");
  tcase_add_checked_fixture(t, loopback_setup, loopback_teardown);
  tcase_add_test(t, types_null_a);
  tcase_add_test(t, types_null_b);
  tcase_add_test(t, types_simple_string);
  tcase_add_test(t, types_error);
  tcase_add_test(t, types_int);
  tcase_add_test(t, types_inline_command);
  tcase_add_test(t, types_bulk_string);
  tcase_add_test(t, types_array);
  tcase_add_test(t, types_invalid);
  tcase_add_test(t, types_empty);
  suite_add_tcase(s,t);

  t = tcase_create("limits");
  tcase_add_checked_fixture(t, loopback_setup, loopback_teardown);
  tcase_add_test(t, limits_int_max);
  tcase_add_test(t, limits_int_max_overflow);
  tcase_add_test(t, limits_int_min);
  tcase_add_test(t, limits_int_min_overflow);
  tcase_add_test(t, limits_bulk_string_overflow);
  tcase_add_test(t, limits_bulk_string_max);
  tcase_add_test(t, limits_bulk_string_min);
  tcase_add_test(t, limits_array_overflow);
  tcase_add_test(t, limits_array_min);
  suite_add_tcase(s,t);

  t = tcase_create("token");
  tcase_add_checked_fixture(t, loopback_setup, loopback_teardown);
  tcase_add_test(t, token_cr);
  tcase_add_test(t, token_lf);
  tcase_add_test(t, token_bulk_crlf);
  tcase_add_test(t, token_split);
  tcase_add_test(t, token_make_room);
  tcase_add_test(t, token_huge_string);
  tcase_add_test(t, token_huge_error);
  tcase_add_test(t, token_huge_int);
  tcase_add_test(t, token_huge_inline);
  tcase_add_test(t, token_huge_invalid);
  tcase_add_test(t, token_split_block);
  tcase_add_test(t, token_huge_next);
  suite_add_tcase(s,t);

  t = tcase_create("io");
  tcase_add_checked_fixture(t, loopback_setup, loopback_teardown);
  tcase_add_test(t, io_fail);
  tcase_add_test(t, io_fail_huge);
  suite_add_tcase(s,t);

  t = tcase_create("misc");
  tcase_add_checked_fixture(t, loopback_setup, loopback_teardown);
  tcase_add_test(t, misc_array_deep_error);
  suite_add_tcase(s,t);

  return s;
}

int main() {
  SRunner* r = srunner_create(mk_suite());
  srunner_run_all(r, CK_NORMAL);
  int f = srunner_ntests_failed(r);
  srunner_free(r);
  return f ? EXIT_FAILURE : EXIT_SUCCESS;
}
