#ifndef __UNIT_UTILS_H__
#define __UNIT_UTILS_H__

#include <libresp.h>

extern struct resp_io* io;
extern struct resp_ctx* ctx;

void loopback_setup();
void loopback_setup_noctx();
void loopback_setup_attr(struct resp_attr*);
void loopback_teardown();

struct resp_io* loopback_io_init();
void loopback_io_force_eof(struct resp_io*);

#endif
