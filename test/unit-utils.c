#include <libresp.h>
#include <libresp-private.h>

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#if defined(RESP_HOST_WIN32)
#include <windows.h>
#endif

static inline int open_pipe(resp_fd* fd) {
#if defined(RESP_HOST_WIN32)
  return CreatePipe(fd, fd + 1, NULL, 0) ? 0 : -1;
#else
  return pipe(fd);
#endif
}

static inline void close_pipe(resp_fd fd) {
#if defined(RESP_HOST_WIN32)
  CloseHandle(fd);
#else
  close(fd);
#endif
}


struct resp_io* loopback_io_init() {
  resp_fd fd[2];

  if(open_pipe(fd) == -1)
    return NULL;

  return resp_io_fd_init(fd[0], fd[1]);
}

struct resp_io* io = NULL;
struct resp_ctx* ctx = NULL;
static void(*ctx_free)(struct resp_ctx*) = resp_free;

void loopback_setup() {
  io = loopback_io_init();
  assert(io);
  ctx = resp_init(io, NULL);
  assert(ctx);
}

static void loopback_free_noctx(struct resp_ctx* ctx) {
  resp_io_free(ctx->io);
  free(ctx);
}

/* Make a "fake" context with no worker thread */
void loopback_setup_noctx() {
  io = loopback_io_init();
  assert(io);
  ctx = calloc(1, sizeof(struct resp_ctx));
  ctx->io = io;
  ctx_free = loopback_free_noctx;
  assert(ctx);
}

void loopback_setup_attr(struct resp_attr* attr) {
  io = loopback_io_init();
  assert(io);
  ctx = resp_init(io, attr);
  assert(ctx);
}

void loopback_teardown() {
  ctx_free(ctx);
  ctx = NULL;
  io = NULL;
}

void loopback_io_force_eof(struct resp_io* io) {
  resp_fd fd[2];
  resp_io_get_fd(io, fd);
  close_pipe(fd[1]);
}
