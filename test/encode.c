#include <libresp.h>
#include <libresp-private.h>
#include <unit-utils.h>

#include <check.h>
#include <stdlib.h>

static void _check(const char* encoded, size_t len) {
  char* buf = malloc(len);
  size_t r = 0;
  while(r < len) {
    ssize_t nr = resp_io_read(io, buf + r, len - r);
    ck_assert_int_gt(nr, 0);
    r += nr;
  }
  ck_assert_int_eq(r, len);
  ck_assert_mem_eq(buf, encoded, len);
  free(buf);
}

#define check(e) _check((e), sizeof(e) - 1)

static void _check_msg(const struct resp_msg* msg, const char* encoded, size_t len) {
  int w = resp_write(ctx, msg);
  ck_assert_int_eq(w, 0);

  _check(encoded, len);
}

#define check_msg(m,e) _check_msg((m), (e), sizeof(e) - 1)

static struct resp_msg* _mk_string(enum resp_type t, const char* s, size_t len) {
  struct resp_msg* msg = malloc(sizeof(struct resp_msg) + len);
  ck_assert_ptr_ne(msg, NULL);
  msg->type = t;
  msg->length = len - 1;
  memcpy(msg->string, s, len);
  return msg;
}

#define mk_string(t,s) _mk_string((resp_type_ ## t), (s), sizeof(s))

START_TEST(types_null) {
  struct resp_msg msg = { .type = resp_type_null, .null_type = 0 };
  check_msg(&msg, "$-1\r\n");
} END_TEST

START_TEST(types_integer) {
  struct resp_msg msg = { .type = resp_type_integer, .integer = 42 };
  check_msg(&msg, ":42\r\n");
} END_TEST

START_TEST(types_simple_string) {
  struct resp_msg* msg = mk_string(simple_string, "foo");
  check_msg(msg, "+foo\r\n");
  resp_msg_free(msg);
} END_TEST

START_TEST(types_error) {
  struct resp_msg* msg = mk_string(error, "bar");
  check_msg(msg, "-bar\r\n");
  resp_msg_free(msg);
} END_TEST

START_TEST(types_inline_command) {
  struct resp_msg* msg = mk_string(inline_command, "baz");
  check_msg(msg, "baz\r\n");
  resp_msg_free(msg);
} END_TEST

START_TEST(types_bulk_string) {
  struct resp_msg* msg = mk_string(bulk_string, "foobarbaz");
  check_msg(msg, "$9\r\nfoobarbaz\r\n");
  resp_msg_free(msg);
} END_TEST

START_TEST(types_array) {
  struct resp_msg* msg = malloc(sizeof(struct resp_msg) + 3 * sizeof(void*));
  msg->type = resp_type_array;
  msg->num = 3;
  msg->array[0] = mk_string(simple_string, "Hello");
  msg->array[1] = mk_string(simple_string, "World");
  msg->array[2] = mk_string(simple_string, "!");
  check_msg(msg, "*3\r\n+Hello\r\n+World\r\n+!\r\n");
  resp_msg_free(msg);
} END_TEST

START_TEST(writef_example) {
 char b[] = {0,32,64,127};
 resp_int d = 19;
 int i = 42;
 resp_writef(ctx, "sdbi", "PING", d, sizeof(b), b, i);

 check("*4\r\n$4\r\nPING\r\n$2\r\n19\r\n$4\r\n\x00\x20\x40\x7F\r\n$2\r\n42\r\n");
} END_TEST

Suite* mk_suite() {
  TCase* t;
  Suite* s = suite_create("encode");

  t = tcase_create("types");
  tcase_add_checked_fixture(t, loopback_setup_noctx, loopback_teardown);
  tcase_add_test(t, types_null);
  tcase_add_test(t, types_integer);
  tcase_add_test(t, types_simple_string);
  tcase_add_test(t, types_error);
  tcase_add_test(t, types_inline_command);
  tcase_add_test(t, types_bulk_string);
  tcase_add_test(t, types_array);
  suite_add_tcase(s,t);

  t = tcase_create("writef");
  tcase_add_checked_fixture(t, loopback_setup_noctx, loopback_teardown);
  tcase_add_test(t, writef_example);
  suite_add_tcase(s,t);

  return s;
}

int main() {
  SRunner* r = srunner_create(mk_suite());
  srunner_run_all(r, CK_NORMAL);
  int f = srunner_ntests_failed(r);
  srunner_free(r);
  return f ? EXIT_FAILURE : EXIT_SUCCESS;
}
