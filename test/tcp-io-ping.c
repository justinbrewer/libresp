#include <libresp.h>
#include <stdio.h>

int main(int argc, char** argv) {
  if(argc != 3) {
    fprintf(stderr, "usage: %s <ip> <port>\n", argv[0]);
    return 1;
  }

  struct resp_io* io = resp_io_tcp_init(argv[1], argv[2]);

  if(io == NULL) {
    perror("resp_io_tcp_init");
    return 1;
  }

  { const char buf[] = "PING\r\n";
    io->actions->write(io, buf, sizeof(buf));
  }

  { char buf[256] = {0};
    io->actions->read(io, buf, sizeof(buf));
    printf("%s", buf);
  }

  resp_io_free(io);
  return 0;
}
